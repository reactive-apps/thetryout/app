import React from "react";
import ReactDOM from "react-dom";
import connect from "@vkontakte/vkui-connect";
import '@vkontakte/vkui/dist/vkui.css';

import { Provider } from "react-redux";
import store from "./store";

import App from "./App";

import { login } from "./api";

// Init VK App
connect.send("VKWebAppInit", {});
login(window.location.href)
    .then(async () => {
        window.addEventListener("popstate", e => e.preventDefault() & store.dispatch.navigator.goBack(true));
        await store.dispatch.user.load();
        store.dispatch.services.load();
        store.dispatch.navigator.goForce("home");
    })
    .catch(console.error);

// Для мамкиных хакеров
console.log("%cСука, не трогай продакш!!1! Здесь и так все на соплях. Сломаешь - сам будешь фиксить. %cИ да, мы просто не хотим фиксить баги, так что разместили это сообщение тут %c ", "color: red; font-size:25px", "background:black ; color: white", "font-size:400px; background:url(https://pics.me.me/codeit-google-until-youfinda-stackoverflow-answerwith-code-to-copy-paste-34126823.png) no-repeat;");

ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById("root"));
