import React from "react";

export const renderPriority = (priority) => {
    switch (priority) {
        case "low": return "Низкий";
        case "middle": return "Средний";
        case "high": return "Высокий";
        case "critical": return "Критический";
        case "vulnerability": return "Уязвимость";
        default: return "-";
    }
};

export const renderStatus = (status) => {
    switch (status) {
        case "open": return "Открыт";
        case "cancelled": return "Закрыт";
        case "review": return "На рассмотрении";
        case "rejected": return "Отклонён";
        case "wont-fix": return "wont fix";
        default: return "-";
    }
};

export const renderType = (type) => {
    switch (type) {
        case "not-work": return "Не работает";
        case "lock": return "Зависание";
        case "spelling": return "Ошибка в тексте";
        case "feature": return "Предложение";
        case "catched-error": return "Пойманная ошибка";
        default: return "-";
    }
};

export const nl2br = (text) => {
    return text.split('\n').map((item, key) => {
        return (
            <span key={key}>
                {item}
                <br/>
            </span>
        )
    })
};