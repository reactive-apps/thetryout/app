import React from "react";
import { connect } from "react-redux";
import { Panel, PanelHeader, Group, Avatar, Cell, Div, Button, Alert, Select } from "@vkontakte/vkui";
import { renderPriority, renderType, renderStatus, nl2br } from "../text-render";
import FireOpener from "../fire-opener";
import Counter from "@vkontakte/vkui/dist/components/Counter/Counter";
import Icon24Info from '@vkontakte/icons/dist/24/info';
import PanelHeaderBack from '@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack';

const Report = ({ id, report, service_version, goBack, goForward, deleteReport, actualReport, setPopout, isServiceDeveloper, updateStatus }) => {
    const updateStatusAlert = () => {
        let newStatus = report.status;
        return setPopout(
            <Alert
                actions={[
                    {
                        title: "Отмена",
                        autoclose: true,
                        style: "cancel"
                    },
                    {
                        title: "Сохранить",
                        action: () => updateStatus({ report, newStatus })
                    }
                ]}
                onClose={() => setPopout(null)}
            >
                <h2>Выберите статус</h2>
                <Select
                    top="Статус"
                    value={newStatus || ""}
                    onChange={(e) => newStatus = e.currentTarget.options[e.currentTarget.selectedIndex].value}
                >
                    <option value="open">Открыт</option>
                    <option value="review">На рассмотрении</option>
                    <option value="cancelled">Закрыт</option>
                    <option value="rejected">Отклонён</option>
                    <option value="wont-fix">wont fix</option>
                </Select>
            </Alert>
        );
    };

    return (
        <Panel id={id}>
            <PanelHeader left={<PanelHeaderBack onClick={goBack}/>}>Отчёт</PanelHeader>
            {report.type === "catched-error" ?
                <div>
                    {report.error ?
                        <Group
                            title="Ошибка"
                            children={
                                <Div style={{ paddingTop: 0 }}>
                                    {nl2br(report.error.toString())}
                                </Div> }
                        />
                    : null}
                    {report.message ?
                        <Group
                            title="Комментарий пользователя"
                            children={
                                <Div style={{ paddingTop: 0 }}>
                                    {nl2br(report.message.toString())}
                                </Div> }
                        />
                    : null}
                </div>
            : null}
            {report.type !== "catched-error" ?
                <div>
                    <Group
                        title="Проблема"
                        children={
                            <Div style={{ paddingTop: 0 }}>
                                <div style={{ fontSize: 18, fontWeight: "bold" }}>{report.title}</div>
                                <div>Шаги воспроизведения:</div>
                                <div>{nl2br(report.steps)}</div>
                            </Div>
                        }
                    />
                    <Group title="Фактический результат" children={<Div style={{ paddingTop: 0 }} children={nl2br(report.actual_result)}/>} />
                    <Group title="Ожидаемый результат" children={<Div style={{ paddingTop: 0 }} children={nl2br(report.expected_result)}/>} />
                </div>
            : null}
            <Group title="Сведения">
                <Cell
                    children="Версия продукта"
                    indicator={<Counter type="primary" children={report.version} />}
                />
                {report.platform ?
                    <Cell
                        children="Платформа"
                        indicator={report.platform === "ios" ? "iOS" : report.platform === "android" ? "Android" : "Desktop"}
                    />
                    : null}
                <Cell
                    children="Статус"
                    indicator={
                        <div style={{ display: "flex" }} onClick={() => goForward("status-help")}>
                            <span style={{ paddingRight: 5 }}>{renderStatus(report.status)}</span>
                            <Icon24Info fill="var(--counter_primary_background)"/>
                        </div>
                    }
                />
                <Cell
                    children="Тип"
                    asideContent={renderType(report.type)}
                />
                <Cell
                    children="Приоритет"
                    asideContent={renderPriority(report.priority)}
                />
            </Group>
            {report.creatorData && report.creatorData.first_name ?
                <Group title="Автор">
                    <Cell
                        expandable
                        children={report.creatorData.first_name + " " + report.creatorData.last_name}
                        before={<Avatar src={report.creatorData.photo_200}/>}
                        onClick={() => FireOpener("https://vk.com/id" + report.creator)}
                    />
                </Group>
                : null}

            {(report.isMy && report.status !== "cancelled") || isServiceDeveloper ?
                <Group title="Действия">
                    <Div>
                        {report.isMy && report.version !== service_version && report.status !== "cancelled" ?
                            <Button
                                size="xl"
                                children={"Актуален для " + service_version}
                                style={{marginBottom: 10}}
                                onClick={() => actualReport(report)}
                            />
                            : null}
                        {report.isMy && report.status === "open" ?
                            <Button
                                size="xl"
                                children="Редактировать отчёт"
                                style={{marginBottom: 10}}
                                onClick={() => goForward("update-report")}
                            />
                            : null}
                        {isServiceDeveloper ?
                            <Button
                                size="xl"
                                children="Обновить статус"
                                style={{marginBottom: 10}}
                                onClick={() => updateStatusAlert()}
                            />
                            : null}
                        {report.status === "open" || isServiceDeveloper ?
                            <Button
                                size="xl"
                                children="Удалить отчёт"
                                onClick={() => deleteReport({service_id: report.service_id, report_id: report.id})}
                            />
                            : null}
                    </Div>
                </Group>
                : null}
        </Panel>
    );
};

const mapState = (state) => ({
    report: state.reports.activeReport,
    service_version: state.services.activeService.version,
    isServiceDeveloper: state.services.activeService.isServiceDeveloper
});

const mapDispatch = (dispatch) => ({
    goBack: dispatch.navigator.goBack,
    goForward: dispatch.navigator.goForward,
    setPopout: dispatch.navigator.setPopout,
    deleteReport: dispatch.reports.deleteReport,
    actualReport: dispatch.reports.actualReport,
    updateStatus: dispatch.reports.updateStatus
});

export default connect(mapState, mapDispatch)(Report);