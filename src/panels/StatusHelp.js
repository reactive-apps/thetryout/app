import React from "react";
import { connect } from "react-redux";
import { Panel, PanelHeader, Div } from "@vkontakte/vkui";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";

import Icon24Cancel from '@vkontakte/icons/dist/24/cancel';
import Icon24Done from '@vkontakte/icons/dist/24/done';
import Icon24Recent from '@vkontakte/icons/dist/24/recent';
import Icon24View from '@vkontakte/icons/dist/24/view';
import Icon24Smile from '@vkontakte/icons/dist/24/smile';

const StatusCard = ({ icon, title, description }) => (
    <Div style={{ display: "flex", alignItems: "center" }}>
        {icon}
        <div style={{ paddingLeft: 10 }}>
            <p
                style={{
                    fontSize: 18,
                    fontWeight: "bold",
                    marginTop: 5,
                    marginBottom: 5,
                    color: "var(--text_primary)"
                }}
                children={title}
            >

            </p>
            <p
                style={{
                    color: "var(--text_secondary)",
                    margin: 0
                }}
                children={description}
            />
        </div>
    </Div>
);

const StatusHelp = ({ id, goBack }) => (
    <Panel id={id} theme="white">
        <PanelHeader
            left={<PanelHeaderBack onClick={goBack}/>}
        >
            Статусы отчётов
        </PanelHeader>
        <StatusCard
            icon={<Icon24Recent fill="var(--counter_primary_background)"/>}
            title="Отчёт ожидает проверки"
            description="В это время вы можете отредактировать отчёт, если заметили неточность, или удалить, если что-то перепутали."
        />
        <StatusCard
            icon={<Icon24View fill="var(--counter_primary_background)"/>}
            title="Отчёт рассматривается разработчиком"
            description="Когда разработчик ставит этот статус, это означает, что он работает над этим отчётом. Нужно немного подождать"
        />
        <StatusCard
            icon={<Icon24Done fill="var(--counter_primary_background)"/>}
            title="Отчёт закрыт"
            description="Разработчик устранил неисправность. Отлично!"
        />
        <StatusCard
            icon={<Icon24Cancel fill="var(--destructive)"/>}
            title="Отчёт отклонён"
            description="Разработчик не принял отчёт."
        />
        <StatusCard
            icon={<Icon24Smile fill="var(--counter_primary_background)"/>}
            title="Won't fix"
            description="Разработчик не может устранить неисправность, либо в текущий момент это сделать невозможно. Так же этот статус очень часто ассоциируют с фразой 'Не баг, а фича'."
        />
    </Panel>
);

const mapState = () => ({

});

const mapDispatch = (dispatch) => ({
    goBack: dispatch.navigator.goBack
});

export default connect(mapState, mapDispatch)(StatusHelp);