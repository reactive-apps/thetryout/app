import React, { useState } from "react";
import { connect } from "react-redux";
import { Panel, PanelHeader, FormLayout, FormStatus, Input, FixedLayout, Button } from "@vkontakte/vkui";
import PanelHeaderBack from '@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack';

const Join = ({ id, goBack, join }) => {
    const [ invite, setInvite ] = useState("");
    const [ error, setError ] = useState(false);

    const joinOn = () => {
        setError(null);
        join({
            invite: invite,
            errorCallback: () => setError({
                title: "Ошибка",
                message: "Неверный инвайт или тестирования не существует"
            })
        });
    };

    return (
        <Panel id={id} theme="white">
            <PanelHeader
                left={<PanelHeaderBack onClick={goBack}/>}
            >
                Присоединиться
            </PanelHeader>
            <FormLayout>
                <FormStatus title="Введите инвайт-код" state="default">
                    Чтобы присоединиться к тестированию закрытого сервиса вам необходимо ввести код приглашения.
                    Обычно его предоставляют сами разработчики.
                </FormStatus>
                { error ? <FormStatus title={error.title} children={error.message} state="error" /> : null }
                <Input
                    top="Код"
                    placeholder="*********"
                    value={invite}
                    onChange={(e) => setInvite(e.currentTarget.value.trim())}
                />
            </FormLayout>
            <FixedLayout vertical="bottom" style={{ marginBottom: 15, marginLeft: 15, paddingRight: 30  }}>
                <Button size="xl" onClick={() => joinOn()}>Присоединиться</Button>
            </FixedLayout>
        </Panel>
    );
};

const mapState = () => ({

});

const mapDispatch = (dispatch) => ({
    goBack: dispatch.navigator.goBack,
    join: dispatch.services.joinByInvite
});

export default connect(mapState, mapDispatch)(Join);