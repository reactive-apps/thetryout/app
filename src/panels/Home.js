import React from "react";
import { connect } from "react-redux";
import { Panel, PanelHeader, HeaderButton, Group, List, Cell, Avatar, Spinner, PullToRefresh } from "@vkontakte/vkui";
import Counter from "@vkontakte/vkui/dist/components/Counter/Counter";

import Icon24Add from "@vkontakte/icons/dist/24/add";
import IconVerified12 from "@vkontakte/icons/dist/12/verified";

const Home = ({ id, developer, goForward, services, servicesLoad, selectService, servicesReload, selectDeveloper }) => (
    <Panel id={id}>
        <PanelHeader
            left={<HeaderButton onClick={() => goForward("join")} children={<Icon24Add/>}/>}
        >
            TryOut
        </PanelHeader>
        <PullToRefresh onRefresh={servicesReload} isFetching={servicesLoad}>
            <div>
                {developer ?
                    <Group>
                        <Cell
                            expandable
                            children={
                                <div style={{ display: "flex", alignItems: "center" }}>
                                    <span>{developer.name}</span>
                                    {!!developer.verified
                                        ? <IconVerified12 style={{ marginLeft: 5, marginTop: 2 }} fill="var(--counter_primary_background)"/>
                                        : null
                                    }
                                </div>
                            }
                            style={{ paddingTop: 5, paddingBottom: 5 }}
                            description="Разработчик"
                            onClick={() => selectDeveloper(developer)}
                            before={<Avatar src={developer.icon || "https://vk.com/images/vkapp_i.png"} size={64}/>}
                        />
                    </Group>
                : null}
                <Group title="Мои сервисы">
                    {servicesLoad ?
                        services.filter((x) => x.joined).length >= 1 ?
                            <List>
                                {services.filter((x) => x.joined).map((service, key) => (
                                        <Cell
                                            expandable
                                            key={key}
                                            children={service.name}
                                            onClick={() => selectService(service.id)}
                                            indicator={<Counter children={service.version} type="primary"/>}
                                            before={<Avatar type="app" src={service.icon || "https://vk.com/images/vkapp_i.png"}/>}
                                            description={
                                                <div>
                                                    <div style={{ display: "flex", alignItems: "center" }}>
                                                        <span>Разработчик: {service.developer.name}</span>
                                                        {!!service.developer.verified
                                                            ? <IconVerified12 style={{ marginLeft: 5, marginTop: 1 }} fill="var(--counter_primary_background)"/>
                                                            : null
                                                        }
                                                    </div>
                                                </div>
                                            }
                                        />
                                    )
                                )}
                            </List>
                        : <div style={{ paddingBottom: 15, textAlign: "center", color: "var(--text_secondary)" }}>Вы не учавствуйте ни в одном тестировании</div>
                    : <div style={{ paddingBottom: 15 }}><Spinner/></div>}
                </Group>
                <Group title="Все сервисы">
                    {servicesLoad ?
                        services.filter((x) => !x.joined).filter((x) => !!x.public).length >= 1 ?
                            <List>
                                {services.filter((x) => !x.joined).map((service, key) => (
                                        <Cell
                                            expandable
                                            key={key}
                                            children={service.name}
                                            onClick={() => selectService(service.id)}
                                            indicator={<Counter children={service.version} type="primary" />}
                                            before={<Avatar type="app" src={service.icon || "https://vk.com/images/vkapp_i.png"} />}
                                            description={
                                                <div>
                                                    <div style={{ display: "flex", alignItems: "center" }}>
                                                        <span>Разработчик: {service.developer.name}</span>
                                                        {!!service.developer.verified
                                                            ? <IconVerified12 style={{ marginLeft: 5, marginTop: 1 }} fill="var(--counter_primary_background)"/>
                                                            : null
                                                        }
                                                    </div>
                                                </div>
                                            }
                                        />
                                    )
                                )}
                            </List>
                            : <div style={{ paddingBottom: 15, textAlign: "center", color: "var(--text_secondary)" }}>Вы супергерой, спасающий мир</div>
                    : <div style={{ paddingBottom: 15 }}><Spinner/></div>}
                </Group>
            </div>
        </PullToRefresh>
    </Panel>
);

const mapState = (state) => ({
    services: state.services.list,
    servicesLoad: state.services.isLoaded,
    developer: state.user.developer
});

const mapDispatch = (dispatch) => ({
    goForward: dispatch.navigator.goForward,
    selectService: dispatch.services.selectService,
    servicesReload: dispatch.services.load,
    selectDeveloper: dispatch.developer.selectDeveloper
});

export default connect(mapState, mapDispatch)(Home);