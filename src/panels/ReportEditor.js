import React, { useState } from "react";
import { connect } from "react-redux";
import { Panel, PanelHeader, FormLayout, Textarea, Select, Button, FormStatus, platform as osname, IOS } from "@vkontakte/vkui";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";

const Report = ({ id, service_id, version, goBack, action, existingData, actionType }) => {
    const [ title, setTitle ] = useState(existingData.title || "");
    const [ steps, setSteps ] = useState(existingData.steps || "");
    const [ actualResult, setActualResult ] = useState(existingData.actual_result || "");
    const [ expectedResult, setExpectedResult ] = useState(existingData.expected_result || "");
    const [ type, setType ] = useState(existingData.type || "");
    const [ priority, setPriority ] = useState(existingData.priority || "");
    const [ platform, setPlatform ] = useState(existingData.platform ? existingData.platform : osname() === IOS ? "ios" : "android");

    // title, state, children
    const [ message, setMessage ] = useState(null);

    const sendReport = () => {
        setMessage(null);

        if ([ title, steps, actualResult, expectedResult, type, priority ].indexOf("") !== -1) {
            return setMessage({
                title: "Заполните все поля",
                state: "error",
                children: "Чтобы отправить отчёт необходимо заполнить все поля"
            });
        }

        const payload = {
            title,
            steps,
            actual_result: actualResult,
            expected_result: expectedResult,
            type,
            priority,
            version,
            platform,
            status: existingData.status
        };
        action({
            report: actionType === "create" ? payload : {
                ...Object.keys(existingData).filter((x) => payload[x])
                    .reduce((a, x) => { a[x] = existingData[x]; return a; }, {
                        id: existingData.id,
                        creator: existingData.creator,
                        created_at: existingData.created_at
                    }),
                ...payload
            },
            service_id,
            errorCallback: () => setMessage({
                title: "Что-то указано неверно",
                state: "error",
                children: "Проверьте заполненные данные"
            })
        });
    };

    return (
        <Panel id={id} theme="white">
            <PanelHeader
                left={<PanelHeaderBack onClick={goBack}/>}
            >
                {actionType === "create" ? "Создать отчёт" : "Редактировать отчёт" }
            </PanelHeader>
            <FormLayout>
                {message ? <FormStatus {...message} /> : null}
                <Textarea
                    top="Заголовок"
                    value={title}
                    status={message && message.state === "error" && title === "" ? "error" : "default"}
                    placeholder="Не отображается 1000$ в кошельке..."
                    onChange={(e) => setTitle(e.currentTarget.value)}
                />
                <Textarea
                    top="Шаги воспроизведения"
                    value={steps}
                    status={message && message.state === "error" && steps === "" ? "error" : "default"}
                    placeholder="1. Зайти в кошелек..."
                    onChange={(e) => setSteps(e.currentTarget.value)}
                />
                <Textarea
                    top="Фактический результат"
                    value={actualResult}
                    status={message && message.state === "error" && actualResult === "" ? "error" : "default"}
                    placeholder="Отображается 0$"
                    onChange={(e) => setActualResult(e.currentTarget.value)}
                />
                <Textarea
                    top="Ожидаемый результат"
                    value={expectedResult}
                    status={message && message.state === "error" && expectedResult === "" ? "error" : "default"}
                    placeholder="Отображается 100$"
                    onChange={(e) => setExpectedResult(e.currentTarget.value)}
                />
                <Select
                    top="Платформа"
                    placeholder="Выберите платформу"
                    status={message && message.state === "error" && platform === "" ? "error" : "default"}
                    value={platform}
                    onChange={(e) => setPlatform(e.currentTarget.options[e.currentTarget.selectedIndex].value)}
                >
                    <option value="ios">iOS</option>
                    <option value="android">Android</option>
                    <option value="desktop">Desktop</option>
                </Select>
                <Select
                    top="Тип"
                    placeholder="Выберите тип отчёта"
                    status={message && message.state === "error" && type === "" ? "error" : "default"}
                    value={type}
                    onChange={(e) => setType(e.currentTarget.options[e.currentTarget.selectedIndex].value)}
                >
                    <option value="not-work">Не работает</option>
                    <option value="lock">Зависание</option>
                    <option value="spelling">Ошибка в тексте</option>
                    <option value="feature">Предложение</option>
                </Select>
                <Select
                    top="Приоритет"
                    placeholder="Выберите приоритет отчёта"
                    status={message && message.state === "error" && priority === "" ? "error" : "default"}
                    value={priority}
                    onChange={(e) => setPriority(e.currentTarget.options[e.currentTarget.selectedIndex].value)}
                >
                    <option value="low">Низкий</option>
                    <option value="middle">Средний</option>
                    <option value="high">Высокий</option>
                    <option value="critical">Критический</option>
                    <option value="vulnerability">Уязвимость</option>
                </Select>
                <Button size="xl" onClick={() => sendReport()}>{actionType === "create" ? "Создать" : "Сохранить"}</Button>
            </FormLayout>
        </Panel>
    );
};

export default {
    create: connect(
        (state) => ({
            service_id: state.services.activeService.id,
            version: state.services.activeService.version,
            existingData: {},
            actionType: "create"
        })
        ,
        (dispatch) => ({
            goBack: dispatch.navigator.goBack,
            action: dispatch.reports.createReport
        })

    )(Report),
    update: connect(
        (state) => ({
            service_id: state.services.activeService.id,
            version: state.services.activeService.version,
            existingData: state.reports.activeReport,
            actionType: "update"
        }),
        (dispatch) => ({
            goBack: dispatch.navigator.goBack,
            action: dispatch.reports.updateReport
        })
    )(Report)
};