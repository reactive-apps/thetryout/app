import React from "react";
import { Panel, Spinner } from "@vkontakte/vkui";

const Init = ({ id }) => (
    <Panel
        id={id}
        theme="white"
    >
        <Spinner/>
    </Panel>
);

export default Init;