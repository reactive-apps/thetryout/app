import React, { useState } from "react";
import { connect } from "react-redux";
import { FormLayout, Input, Button, FormStatus, Group, Cell, List, CellButton } from "@vkontakte/vkui";

const Settings = ({ developer, updateDeveloper }) => {
    const [ name, setName ] = useState(developer.name);
    const [ bio, setBio ] = useState(developer.bio);
    const [ icon, setIcon ] = useState(developer.icon);
    const [ links, setLinks ] = useState(developer.links || []);
    const [ message, setMessage ] = useState(false);

    const moveLink = ({ from, to }) => {
        const draggingList = [...links];
        draggingList.splice(from, 1);
        draggingList.splice(to, 0, links[from]);
        setLinks(draggingList);
    };

    const setLinkData = (id, data) => {
        const changingList = [...links];
        changingList[id] = { ...links[id], ...data };
        setLinks(changingList);
        return true;
    };

    const addLink = () => setLinks([ { "text": "", "link": "" }, ...links ]);
    const deleteLink = (id) => setLinks(links.filter((x, key) => key !== id));

    const sendDeveloper = () => {
        setMessage(false);
        updateDeveloper({
            developer: {
                name,
                icon,
                bio,
                links
            },
            successCallback: () => setMessage({
                title: "Настройки были обновлены",
                state: "default",
                style: { background: "var(--counter_primary_background)", color: "var(--counter_primary_text)" },
                children: "Данные разработчика успешно обновлены"
            }),
            errorCallback: () => setMessage({
                title: "Что-то указано неверно",
                state: "error",
                children: "Проверьте заполненные данные"
            })
        });
    };

    return (
        <FormLayout>
            {message ? <FormStatus {...message}/> : null}
            <Input
                top="Название"
                value={name}
                status={message && message.state === "error" && name === "" ? "error" : "default"}
                onChange={(e) => setName(e.currentTarget.value)}
            />
            <Input
                top="О себе"
                value={bio}
                placeholder="Расскажите о себе"
                status={message && message.state === "error" && bio === "" ? "error" : "default"}
                onChange={(e) => setBio(e.currentTarget.value)}
            />
            <Input
                top="Иконка"
                value={icon}
                status={message && message.state === "error" && bio === "" ? "error" : "default"}
                onChange={(e) => setIcon(e.currentTarget.value)}
            />
            <Group title="Контакты">
                <CellButton align="center" onClick={() => addLink()}>Добавить ссылку</CellButton>
                <List>
                    {links.map(({ link, text }, key) => (
                        <Cell
                            key={key}
                            draggable
                            removable
                            onDragFinish={moveLink}
                            onRemove={() => deleteLink(key)}
                        >
                            <Input
                                style={{ marginBottom: 10 }}
                                placeholder="Текст ссылки"
                                value={text}
                                onChange={(e) => setLinkData(key, { text: e.currentTarget.value })}
                            />
                            <Input
                                style={{ marginBottom: 10 }}
                                placeholder="Адрес ссылки"
                                value={link}
                                onChange={(e) => setLinkData(key, { link: e.currentTarget.value })}
                            />
                        </Cell>
                    ))}
                </List>
            </Group>
            <Button
                size="xl"
                children="Сохранить"
                onClick={() => sendDeveloper()}
            />
        </FormLayout>
    );
};

const mapState = (state) => ({
    developer: state.developer
});

const mapDispatch = (dispatch) => ({
    updateDeveloper: dispatch.developer.updateDeveloper
});

export default connect(mapState, mapDispatch)(Settings);