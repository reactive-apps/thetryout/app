import React, { useState } from "react";
import { connect } from "react-redux";
import {Cell, HeaderContext, List, Panel, PanelHeader, PanelHeaderContent} from "@vkontakte/vkui";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";

import Icon16Dropdown from '@vkontakte/icons/dist/16/dropdown';
import Icon24Done from '@vkontakte/icons/dist/24/done';
import Icon24User from '@vkontakte/icons/dist/24/user';
import Icon24Settings from '@vkontakte/icons/dist/24/settings';

import Profile from "./Profile";
import Settings from "./Settings";

const Developer = ({ id, developer, goBack }) => {
    const [ isMenuOpen, setIsMenuOpen ] = useState(false);
    const [ menuSelected, setMenuSelected ] = useState("profile");
    const selectMenuItem = (e) => {
        setMenuSelected(e.currentTarget.dataset.mode);
        requestAnimationFrame(() => setIsMenuOpen(!isMenuOpen));
    };

    return (
        <Panel id={id} theme={menuSelected === "settings" ? "white" : "gray"}>
            <PanelHeader
                left={<PanelHeaderBack onClick={goBack}/>}
            >
                <PanelHeaderContent
                    aside={developer.isMy  ? <Icon16Dropdown /> : null}
                    onClick={developer.isMy  ? () =>  setIsMenuOpen(!isMenuOpen) : null}
                >
                    {{
                        profile: developer.isMy ? "Профиль" : developer.name,
                        settings: "Настройки"
                    }[menuSelected]}
                </PanelHeaderContent>
            </PanelHeader>
            <HeaderContext opened={isMenuOpen} onClose={() => setIsMenuOpen(!isMenuOpen)}>
                <List>
                    <Cell
                        before={<Icon24User />}
                        asideContent={menuSelected === "profile" ? <Icon24Done fill="var(--accent)" /> : null}
                        onClick={selectMenuItem}
                        data-mode="profile"
                    >
                        Профиль
                    </Cell>
                    <Cell
                        before={<Icon24Settings />}
                        asideContent={menuSelected === "settings" ? <Icon24Done fill="var(--accent)" /> : null}
                        onClick={selectMenuItem}
                        data-mode="settings"
                    >
                        Настройки
                    </Cell>
                </List>
            </HeaderContext>
            {{
                profile: <Profile/>,
                settings: <Settings/>
            }[menuSelected]}
        </Panel>
    );
};

const mapState = (state) => ({
    developer: state.developer
});

const mapDispatch = (dispatch) => ({
    goBack: dispatch.navigator.goBack
});

export default connect(mapState, mapDispatch)(Developer);