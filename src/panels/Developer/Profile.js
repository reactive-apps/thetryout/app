import React from "react";
import { connect } from "react-redux";
import { Avatar, Cell, Group, CellButton } from "@vkontakte/vkui";
import Counter from "@vkontakte/vkui/dist/components/Counter/Counter";

import FireOpener from "../../fire-opener";
import Icon24Linked from '@vkontakte/icons/dist/24/linked';
import IconVerified12 from "@vkontakte/icons/dist/12/verified";
import Icon24Add from "@vkontakte/icons/dist/24/add";

const Profile = ({ developer, goForward }) => {
    return (
        <div>
            <Group style={{ paddingTop: 10, paddingBottom: 10 }}>
                <Cell
                    children={
                        <div style={{ display: "flex", alignItems: "center" }}>
                            <span>{developer.name}</span>
                            {!!developer.verified
                                ? <IconVerified12 style={{ marginLeft: 5, marginTop: 1 }} fill="var(--counter_primary_background)"/>
                                : null
                            }
                        </div>
                    }
                    description={developer.bio || ""}
                    before={<Avatar src={developer.icon || "https://vk.com/images/vkapp_i.png"} size={80}/>}
                    size="m"
                />
            </Group>
            {developer.staff && developer.staff.length > 0 ?
                <Group title="Сотрудники">
                    {developer.staff.map((user, key) => (
                        <Cell
                            key={key}
                            expandable
                            children={user.first_name + " " + user.last_name}
                            before={<Avatar src={user.photo_200}/>}
                            onClick={() => FireOpener("https://vk.com/id" + user.id)}
                        />
                    ))}
                </Group>
                : null}
            {developer.links && developer.links.length > 0 ?
                <Group title="Контакты">
                    {developer.links.map((link, key) => (
                        <Cell
                            key={key}
                            expandable
                            children={
                                <div
                                    style={{
                                        display: "flex"
                                    }}
                                >
                                    <Icon24Linked fill="var(--icon_tertiary)"/>
                                    <span style={{ color: "var(--text_primary)", paddingLeft: 5 }}>{link.text}</span>
                                </div>
                            }
                            onClick={() => FireOpener(link.link)}
                        />
                    ))}
                </Group>
                : null}
            {(developer.services && developer.services.length > 0) || developer.isMy ?
                <Group title="Сервисы">
                    {developer.services.map((service, key) => (
                        <Cell
                            key={key}
                            children={service.name}
                            before={<Avatar type="app" src={service.icon || "https://vk.com/images/vkapp_i.png"} />}
                            indicator={<Counter children={service.version} type="primary" />}
                            description={service.public ? "Публичное тестирование" : "Закрытое тестирование"}
                        />
                    ))}
                    {developer.isMy ?
                        <CellButton
                            align="center"
                            before={<Icon24Add />}
                            onClick={() => goForward("create-service")}
                        >
                            Добавить сервис
                        </CellButton>
                    : null}
                </Group>
            : null}
        </div>
    );
};

const mapState = (state) => ({
    developer: state.developer
});

const mapDispatch = (dispatch) => ({
    goForward: dispatch.navigator.goForward
});

export default connect(mapState, mapDispatch)(Profile);