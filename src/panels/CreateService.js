import React, { useState } from "react";
import { connect } from "react-redux";
import { Panel, PanelHeader, FormLayout, FormStatus, Input, Button } from "@vkontakte/vkui";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";

const CreateService = ({ id, goBack, createService }) => {
    const [ link, setLink ] = useState("");
    const [ name, setName ] = useState("");
    const [ icon, setIcon ] = useState("");
    const [ message, setMessage ] = useState(null);

    const sendService = () => {
        if([name].indexOf("") !== -1) {
            return setMessage({
                title: "Заполните все поля",
                state: "error",
                children: "Чтобы отправить отчёт необходимо заполнить все поля"
            });
        }

        createService({
            service: {
                link,
                name,
                icon
            },
            errorCallback: () => setMessage({
                title: "Что-то указано неверно",
                state: "error",
                children: "Проверьте заполненные данные"
            })
        })
    };

    return (
        <Panel id={id} theme="white">
            <PanelHeader left={<PanelHeaderBack onClick={() => goBack()} />}>
                Добавление сервиса
            </PanelHeader>
            <FormLayout>
                {message ? <FormStatus {...message}/> : null}
                <Input
                    top="Название"
                    value={name}
                    status={message && message.type === "error" && name === "" ? "error" : "default"}
                    onChange={(e) => setName(e.currentTarget.value)}
                />
                <Input
                    top="Ссылка на сервис"
                    value={link}
                    placeholder="https://vk.com/app123456"
                    status={message && message.type === "error" && link === "" ? "error" : "default"}
                    onChange={(e) => setLink(e.currentTarget.value)}
                />
                <Input
                    top="Иконка"
                    value={icon}
                    placeholder="Необзательно"
                    status={message && message.type === "error" && icon === "" ? "error" : "default"}
                    onChange={(e) => setIcon(e.currentTarget.value)}
                />
                <Button size="xl" onClick={() => sendService()}>Создать</Button>
            </FormLayout>
        </Panel>
    );
};

const mapState = () => ({

});

const mapDispatch = (dispatch) => ({
    goBack: dispatch.navigator.goBack,
    createService: dispatch.services.createService
});

export default connect(mapState, mapDispatch)(CreateService);