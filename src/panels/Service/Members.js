import React, { useState } from "react";
import { connect } from "react-redux";
import {Group, List, Cell, Avatar, Alert, CellButton} from "@vkontakte/vkui";
import Counter from "@vkontakte/vkui/dist/components/Counter/Counter";

import FireOpener from "../../fire-opener";

const Members = ({ members, myId, setPopout, kickMember }) => {
    const kickMemberAction = (member_id) => setPopout(
        <Alert
            actions={[
                {
                    title: "Исключить",
                    action: () => kickMember(member_id),
                    style: "destructive"
                },
                {
                    title: "Отменить",
                    autoclose: true,
                    style: "cancel"
                }
            ]}
            onClose={() => setPopout(null)}
        >
            <h2>Исключить участника?</h2>
            <p>Участник потеряет доступ к закрытому тестированию и не сможет вернуться без приглашения разработчика.</p>
        </Alert>
    );

    const [ manage, setManage ] = useState(false);

    return (
        <div>
            <Group>
                <CellButton
                    align="center"
                    level={manage ? "primary" : "danger"}
                    onClick={() => setManage(!manage)}
                >
                    { manage ? "Закончить" : "Исключить участников" }
                </CellButton>
            </Group>
            <Group title="Все участники">
                <List>
                    {members.map((member) => (
                        <Cell
                            key={member.id}
                            expandable={!manage}
                            removable={(myId !== member.id.toString()) && manage}
                            before={<Avatar src={member.photo_200} />}
                            children={member.first_name + " " + member.last_name}
                            onClick={() => FireOpener("https://vk.com/id" + member.id)}
                            indicator={myId === member.id.toString() ? <Counter type="primary" children="Вы" /> : null}
                            onRemove={() => kickMemberAction(member.id)}
                        />
                    ))}
                </List>
            </Group>
        </div>
    );
};

const mapState = (state) => ({
    members: state.services.activeService.members,
    myId: state.user.id
});

const mapDispatch = (dispatch) => ({
    setPopout: dispatch.navigator.setPopout,
    kickMember: dispatch.services.kickMember
});

export default connect(mapState, mapDispatch)(Members);