import React  from "react";
import { connect } from "react-redux";
import FireOpener from "../../fire-opener";
import { Div, Cell, Avatar, CellButton, Button, Group, Alert, Spinner, FixedLayout, Tabs, TabsItem } from "@vkontakte/vkui";
import { renderPriority, nl2br } from "../../text-render";
import Counter from "@vkontakte/vkui/dist/components/Counter/Counter";

import IconVerified12 from "@vkontakte/icons/dist/12/verified";
import Icon24Error from '@vkontakte/icons/dist/24/error';
import Icon24Cancel from '@vkontakte/icons/dist/24/cancel';
import Icon24Done from '@vkontakte/icons/dist/24/done';
import Icon24Recent from '@vkontakte/icons/dist/24/recent';
import Icon24View from '@vkontakte/icons/dist/24/view';
import Icon24Smile from '@vkontakte/icons/dist/24/smile';
import Icon24LinkCircle from '@vkontakte/icons/dist/24/link_circle';
import Icon24Add from '@vkontakte/icons/dist/24/add';
import Icon16Chevron from '@vkontakte/icons/dist/16/chevron';

const Overview = ({ service, reports, filter, setFilter, isReportsLoading, goForward, leave, setPopout, selectReport, selectDeveloper, join }) => {
    const openApp = () => {
        if (!service.link) {
            return setPopout(
                <Alert
                    actionsLayout="vertical"
                    actions={[{
                        title: "Закрыть",
                        autoclose: true,
                        style: "cancel"
                    }]}
                    onClose={() => setPopout(null)}
                >
                    <h2>Невозможно открыть</h2>
                    <p>Сервис временно недоступен или ссылка на него не задана разработчиком</p>
                </Alert>
            );
        } else {
            return FireOpener(service.link);
        }
    };

    const leaveFromService = () => {
        return setPopout(
            <Alert
                actionsLayout="vertical"
                actions={[
                    {
                        title: "Покинуть",
                        action: () => leave(service.id),
                        style: "destructive"
                    },
                    {
                        title: "Остаться",
                        autoclose: true,
                        style: "cancel"
                    }
                ]}
                onClose={() => setPopout(null)}
            >
                <h2>Покинуть тестрование?</h2>
                <p>Вы не сможете вернуться обратно без разрешения разработчиков, если это тестирование является закрытым</p>
            </Alert>
        );
    };

    const getStatus = (status) => {
        switch (status) {
            case "rejected": return <Icon24Cancel fill="var(--destructive)"/>;
            case "cancelled": return <Icon24Done fill="var(--counter_primary_background)"/>;
            case "review": return <Icon24View fill="var(--counter_primary_background)"/>;
            case "open": return <Icon24Recent fill="var(--counter_primary_background)"/>;
            case "wont-fix": return <Icon24Smile fill="var(--counter_primary_background)"/>;
            default: return null;
        }
    };

    const filterArray = (array) => {
        const badges = (array) => {
            return array.map((x) => ({
                ...x,
                indicator: x.isMy && service.version !== x.version && x.status !== "cancelled"
                    ? <Icon24Error fill="var(--counter_primary_background)"/>
                    : getStatus(x.status)
            }))
        };
        switch (filter) {
            case "my": return badges(array.filter((x) => x.isMy));
            case "all": return badges(array);
            default: return array;
        }
    };

    return (
        <div>
            <Group style={{ paddingTop: 10, paddingBottom: 10 }}>
                <Cell
                    children={service.name}
                    description={
                        <div onClick={() => selectDeveloper(service.developer)}>
                            <div style={{ display: "flex", alignItems: "center" }}>
                                <span>{service.developer.name}</span>
                                {!!service.developer.verified
                                    ? <IconVerified12 style={{ marginLeft: 5, marginTop: 1 }} fill="var(--counter_primary_background)"/>
                                    : null
                                }
                                <Icon16Chevron style={{ marginLeft: 3 }} fill="var(--icon_tertiary)"/>
                            </div>
                        </div>
                    }
                    indicator={<Counter type="primary" children={service.version} />}
                    before={<Avatar type="app" src={service.icon|| "https://vk.com/images/vkapp_i.png"} size={80}/>}
                    size="m"
                />
                <Div style={{display: 'flex'}}>
                    <Button
                        stretched
                        size="l"
                        style={{ marginRight: 8 }}
                        onClick={() => openApp()}
                    >
                        Открыть
                    </Button>
                    {service.joined ?
                        <Button
                            stretched
                            size="l"
                            level="secondary"
                            onClick={() => leaveFromService()}
                        >
                            Покинуть
                        </Button>
                        :
                        <Button
                            stretched
                            size="l"
                            onClick={() => join(service.id)}
                        >
                            Присоединиться
                        </Button>
                    }

                </Div>
            </Group>
            {service.joined && service.versions[service.version] ?
                <Group title="Что нового?">
                    <div style={{ paddingBottom: 15, marginLeft: 12, color: "var(--text_primary)" }}>
                        {nl2br(service.versions[service.version].text)}
                    </div>
                </Group>
                : null}

            {service.joined ?
                <Group>
                    <div style={{ display: "flex" }}>
                        {service.chat_url ?
                            <CellButton
                                align="center"
                                before={<Icon24LinkCircle/>}
                                style={{display: "inline-block"}}
                                onClick={() => FireOpener(service.chat_url)}
                            >
                                Открыть чат
                            </CellButton>
                        : null}
                        <CellButton
                            align="center"
                            before={<Icon24Add/>}
                            style={{ display: "inline-block" }}
                            onClick={() => goForward("create-report")}
                        >
                            Создать отчёт
                        </CellButton>
                    </div>
                </Group>
                : null}

            {service.joined ?
                <Group title="Отчёты" style={{ marginBottom: 60}}>
                    {isReportsLoading ?
                        filterArray(reports.slice(0)).length >= 1 ?
                            filterArray(reports.slice(0)).map((report) => {
                                return (
                                    <Cell
                                        expandable
                                        key={report.id}
                                        children={report.title}
                                        before={report.indicator ? report.indicator : null}
                                        description={"Приоритет: " + renderPriority(report.priority)}
                                        onClick={() => selectReport(report.id)}
                                    />
                                );
                            })
                            :
                            filter === "my" ?
                                <div style={{ paddingBottom: 15, textAlign: "center", color: "var(--text_secondary)" }}>
                                    Список пуст.<br/>
                                    Похоже, что вы пока не создали ни одного отчёта
                                </div>
                                :   <div style={{ paddingBottom: 15, textAlign: "center", color: "var(--text_secondary)" }}>
                                    Отчётов пока нет.<br/>
                                    И тут разраб вдохнул с облегчением :)
                                </div>
                        : <div style={{ paddingBottom: 15 }}><Spinner/></div>}
                </Group>
                : null}

            {service.joined ?
                <FixedLayout vertical="bottom">
                    <Tabs>
                        <TabsItem
                            selected={filter === "my"}
                            onClick={() => setFilter("my")}
                        >
                            Мои отчёты
                        </TabsItem>
                        <TabsItem
                            selected={filter === "all"}
                            onClick={() => setFilter("all")}
                        >
                            Все отчёты
                        </TabsItem>
                    </Tabs>
                </FixedLayout>
                : null}
        </div>
    );
};

const mapState = (state) => ({
    service: state.services.activeService,
    reports: state.reports.list,
    isReportsLoading: state.reports.isLoaded,
    filter: state.user.filter
});

const mapDispatch = (dispatch) => ({
    goBack: dispatch.navigator.goBack,
    goForward: dispatch.navigator.goForward,
    leave: dispatch.services.leave,
    join: dispatch.services.join,
    setPopout: dispatch.navigator.setPopout,
    selectReport: dispatch.reports.selectReport,
    setFilter: dispatch.user.setFilter,
    selectDeveloper: dispatch.developer.selectDeveloper
});

export default connect(mapState, mapDispatch)(Overview);