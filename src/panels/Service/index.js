import React, { useState } from "react";
import { connect } from "react-redux";
import { Panel, PanelHeader, Cell, PanelHeaderContent, HeaderContext, List } from "@vkontakte/vkui";
import PanelHeaderBack from '@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack';

import Icon24Done from '@vkontakte/icons/dist/24/done';

import Icon16Dropdown from '@vkontakte/icons/dist/16/dropdown';
import Icon24Users from '@vkontakte/icons/dist/24/users';
import Icon24Discussions from '@vkontakte/icons/dist/24/discussions';
import Icon24LinkCircle from '@vkontakte/icons/dist/24/link_circle';
import Icon24Replay from '@vkontakte/icons/dist/24/replay';
import Icon24Settings from '@vkontakte/icons/dist/24/settings';

import Overview from "./Overview";
import Versions from "./Versions";
import Members from "./Members";
import Invites from "./Invites";
import Settings from "./Settings";

const Service = ({ id, service, isServiceDeveloper, goBack }) => {
    const [ isMenuOpen, setIsMenuOpen ] = useState(false);
    const [ menuSelected, setMenuSelected ] = useState("reports");
    const selectMenuItem = (e) => {
        setMenuSelected(e.currentTarget.dataset.mode);
        requestAnimationFrame(() => setIsMenuOpen(!isMenuOpen));
    };

    return (
        <Panel id={id} theme={menuSelected === "settings" ? "white" : "gray"}>
            <PanelHeader
                left={<PanelHeaderBack onClick={goBack}/>}
            >
                <PanelHeaderContent
                    aside={isServiceDeveloper ? <Icon16Dropdown /> : null}
                    onClick={isServiceDeveloper ? () =>  setIsMenuOpen(!isMenuOpen) : null}
                >
                    {{
                        reports: isServiceDeveloper ? "Отчёты" : service.name,
                        versions: "Версии",
                        members: "Участники",
                        invites: "Приглашения",
                        settings: "Настройки"
                    }[menuSelected]}
                </PanelHeaderContent>
            </PanelHeader>
            <HeaderContext opened={isMenuOpen} onClose={() => setIsMenuOpen(!isMenuOpen)}>
                <List>
                    <Cell
                        before={<Icon24Discussions />}
                        asideContent={menuSelected === "reports" ? <Icon24Done fill="var(--accent)" /> : null}
                        onClick={selectMenuItem}
                        data-mode="reports"
                    >
                        Отчеты
                    </Cell>
                    <Cell
                        before={<Icon24Replay />}
                        asideContent={menuSelected === "versions" ? <Icon24Done fill="var(--accent)" /> : null}
                        onClick={selectMenuItem}
                        data-mode="versions"
                    >
                        Версии
                    </Cell>
                    <Cell
                        before={<Icon24Users />}
                        asideContent={menuSelected === "members" ? <Icon24Done fill="var(--accent)" /> : null}
                        onClick={selectMenuItem}
                        data-mode="members"
                    >
                        Участники
                    </Cell>
                    <Cell
                        before={<Icon24LinkCircle />}
                        asideContent={menuSelected === "invites" ? <Icon24Done fill="var(--accent)" /> : null}
                        onClick={selectMenuItem}
                        data-mode="invites"
                    >
                        Приглашения
                    </Cell>
                    <Cell
                        before={<Icon24Settings />}
                        asideContent={menuSelected === "settings" ? <Icon24Done fill="var(--accent)" /> : null}
                        onClick={selectMenuItem}
                        data-mode="settings"
                    >
                        Настройки
                    </Cell>
                </List>
            </HeaderContext>
            {{
                reports: <Overview/>,
                versions: <Versions/>,
                members: <Members/>,
                invites: <Invites/>,
                settings: <Settings/>
            }[menuSelected]}
        </Panel>
    );
};

const mapState = (state) => ({
    service: state.services.activeService,
    isServiceDeveloper: state.services.activeService.isServiceDeveloper
});

const mapDispatch = (dispatch) => ({
    goBack: dispatch.navigator.goBack
});

export default connect(mapState, mapDispatch)(Service);
