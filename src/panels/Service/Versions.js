import React, { useState } from "react";
import { connect } from "react-redux";
import { Group, Cell, CellButton, Input, Textarea, Button, FormLayout, FormStatus } from "@vkontakte/vkui";
import Counter from "@vkontakte/vkui/dist/components/Counter/Counter";

const Versions = ({ versions, actualVersion, createVersion }) => {
    const [ isEditor, setIsEditor ] = useState(false);
    const [ newVersion, setNewVersion ] = useState("");
    const [ newChangelog, setNewChangelog ] = useState("");
    const [ message, setMessage ] = useState(null);

    const sendVersion = () => {
        setMessage(null);
        if([ newVersion, newChangelog ].indexOf("") !== -1) return setMessage({
            title: "Что-то не указано",
            state: "error",
            children: "Проверьте указываемые данные"
        });

        createVersion({
            version: { id: newVersion, text: newChangelog },
            successCallback: () => setMessage({
                title: "Версия создана",
                state: "default",
                style: { background: "var(--counter_primary_background)", color: "var(--counter_primary_text)" },
                children: "Новые отчеты будут создаваться именно с ней"
            }),
            errorCallback: () => setMessage({
                title: "Упс!",
                state: "error",
                children: "Похоже версия с таким номером уже существует :("
            })
        })
    };

    return (
        <div>
            {isEditor ?
                <Group title="Создание новой версии">
                    <FormLayout>
                        {message ? <FormStatus {...message} /> : null}
                        <Input
                            top="Новая версия"
                            placeholder="9.9.9"
                            value={newVersion}
                            status={message && message.state === "error" && newVersion === "" ? "error" : "default"}
                            onChange={(e) => setNewVersion(e.currentTarget.value)}
                        />
                        <Textarea
                            top="Что нового?"
                            placeholder="Добавили фичи, пофиксили баги"
                            value={newChangelog}
                            status={message && message.state === "error" && newChangelog === "" ? "error" : "default"}
                            onChange={(e) => setNewChangelog(e.currentTarget.value)}
                        />
                        <Button
                            size="xl"
                            children="Создать"
                            onClick={() => sendVersion()}
                        />
                    </FormLayout>
                </Group>
            : null}
            {!isEditor ?
                <Group>
                    <CellButton align="center" onClick={() => requestAnimationFrame(() => setIsEditor(true))}>
                        Создать новую версию
                    </CellButton>
                </Group>
            : null}
            <Group
                title="Все версии"
            >
                {Object.keys(versions).reverse().map((version, key) => (
                    <Cell
                        key={key}
                        children={version}
                        description={versions[version].text}
                        indicator={version === actualVersion ? <Counter type="primary" children="Текущая" /> : null}
                    />
                ))}
            </Group>
        </div>
    );
};

const mapState = (state) => ({
    versions: state.services.activeService.versions || [],
    actualVersion: state.services.activeService.version || ""
});

const mapDispatch = (dispatch) => ({
    createVersion: dispatch.services.createVersion
});

export default connect(mapState, mapDispatch)(Versions);