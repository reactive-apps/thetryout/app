import React, { useState } from "react";
import { connect } from "react-redux";
import { FormLayout, Input, Button, FormStatus } from "@vkontakte/vkui";

const Settings = ({ service, updateService }) => {
    const [ link, setLink ] = useState(service.link);
    const [ name, setName ] = useState(service.name);
    const [ icon, setIcon ] = useState(service.icon);
    const [ chatUrl, setChatUrl ] = useState(service.chat_url);
    const [ message, setMessage ] = useState(false);

    const sendService = () => {
        setMessage(false);
        updateService({
            service: {
                id: service.id,
                name,
                icon,
                link,
                chat_url: chatUrl,
                developer: service.developer
            },
            successCallback: () => setMessage({
                title: "Настройки обновлены",
                state: "default",
                style: { background: "var(--counter_primary_background)", color: "var(--counter_primary_text)" },
                children: "Данные сервиса успешно обновлены"
            }),
            errorCallback: () => setMessage({
                title: "Что-то указано неверно",
                state: "error",
                children: "Проверьте заполненные данные"
            })
        });
    };

    return (
        <FormLayout>
            {message ? <FormStatus {...message}/> : null}
            <Input
                top="Идентификатор"
                value={service.id}
                onChange={() => false}
            />
            <Input
                top="Название"
                value={name}
                status={message && message.state === "error" && name === "" ? "error" : "default"}
                onChange={(e) => setName(e.currentTarget.value)}
            />
            <Input
                top="Ссылка на сервис"
                value={link}
                placeholder="https://vk.com/app123456"
                status={message && message.state === "error" && link === "" ? "error" : "default"}
                onChange={(e) => setLink(e.currentTarget.value)}
            />
            <Input
                top="Ссылка на чат"
                value={chatUrl}
                placeholder="Необязательно"
                onChange={(e) => setChatUrl(e.currentTarget.value)}
            />
            <Input
                top="Иконка"
                value={icon}
                placeholder="Необзательно"
                onChange={(e) => setIcon(e.currentTarget.value)}
            />
            <Button
                size="xl"
                children="Сохранить"
                onClick={() => sendService()}
            />
        </FormLayout>
    );
};

const mapState = (state) => ({
    service: state.services.activeService
});

const mapDispatch = (dispatch) => ({
    updateService: dispatch.services.updateService
});

export default connect(mapState, mapDispatch)(Settings);