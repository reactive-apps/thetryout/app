import React, { useState } from "react";
import { connect } from "react-redux";
import { Group, Cell, CellButton, Switch, Button, FormLayout, FormStatus } from "@vkontakte/vkui";
import Counter from "@vkontakte/vkui/dist/components/Counter/Counter";

const Invites = ({ invites, createInvite, deleteInvite }) => {
    const [ isEditor, setIsEditor ] = useState(false);
    const [ newIsInfinity, setNewIsInfinity ] = useState(false);
    const [ manage, setManage ] = useState(false);
    const [ message, setMessage ] = useState(null);

    const sendVersion = () => {
        setMessage(null);
        createInvite({
            invite: { infinity: newIsInfinity },
            successCallback: () => setMessage({
                title: "Приглашение создано",
                state: "default",
                style: { background: "var(--counter_primary_background)", color: "var(--counter_primary_text)" },
                children: "Приглашение может быть использовано прямо сейчас"
            }),
            errorCallback: () => setMessage({
                title: "Упс!",
                state: "error",
                children: "Что-то пошло не так"
            })
        })
    };

    return (
        <div>
            {isEditor ?
                <Group title="Создание нового приглашения">
                    <FormLayout>
                        {message ? <FormStatus {...message} /> : null}
                        <Cell asideContent={<Switch onClick={() => setNewIsInfinity(!newIsInfinity)}/>}>
                            Бесконечное приглашение
                        </Cell>
                        <Button
                            size="xl"
                            children="Создать"
                            onClick={() => sendVersion()}
                        />
                    </FormLayout>
                </Group>
            : null}
            {!isEditor ?
                <Group>
                    <CellButton align="center" onClick={() => requestAnimationFrame(() => setIsEditor(true))}>
                        Создать новое приглашение
                    </CellButton>
                </Group>
            : null}
            {invites.length > 0 ?
                <Group>
                    <CellButton
                        align="center"
                        level={manage ? "primary" : "danger"}
                        onClick={() => requestAnimationFrame(() => setManage(!manage))}
                    >
                        { manage ? "Закончить" : "Удалить приглашения" }
                    </CellButton>
                </Group>
            : null}
            <Group
                title="Все приглашения"
            >
                {invites.map((invite, key) => (
                    <Cell
                        removable={manage}
                        onRemove={() => deleteInvite(invite.id)}
                        key={key}
                        children={invite.id}
                        indicator={invite.infinity ? <Counter type="primary" children="Бесконечное" /> : null}
                    />
                ))}
            </Group>
        </div>
    );
};

const mapState = (state) => ({
    invites: state.services.activeService.invites || [],
});

const mapDispatch = (dispatch) => ({
    createInvite: dispatch.services.createInvite,
    deleteInvite: dispatch.services.deleteInvite
});

export default connect(mapState, mapDispatch)(Invites);