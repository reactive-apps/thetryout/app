import connect from "@vkontakte/vkui-connect";
import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/functions";
import "firebase/performance";
import "firebase/auth";
import "firebase/functions";

const firebaseConfig = {
    apiKey: "AIzaSyDSv-RPfqmf5yy5hF6Io04tHUha6TbZzhA",
    authDomain: "tryout-vkapps.firebaseapp.com",
    databaseURL: "https://tryout-vkapps.firebaseio.com",
    projectId: "tryout-vkapps",
    storageBucket: "",
    messagingSenderId: "682428334186",
    appId: "1:682428334186:web:81d3458572f61a8e"
};

firebase.initializeApp(firebaseConfig);
const auth = firebase.auth();
const firestore = firebase.firestore();
const functions = firebase.functions();

/*
    User section
 */
export const login = (url) => new Promise((resolve, reject) => {
    const func = functions.httpsCallable("loginWithUrl");
    func({ url })
        .then((response) => auth.signInWithCustomToken(response.data.access_token)
                .then(() => resolve())
                .catch(e => reject(e))
        )
        .catch((e) => reject(e));
});

export const getAccount = () => new Promise((resolve, reject) => {
    if (!auth.currentUser) return reject();
    firestore.collection("users").doc(auth.currentUser.uid).get()
        .then(async (user) => {
            if (!user.exists) return resolve({});
            let userData = user.data();
            if (userData.developer)
                userData.developer = { ...await userData.developer.get().then((developer) => ({ ...developer.data(), id: developer.id})) };
            if (userData.services)
                userData.services = userData.services.reduce((a, x) => {
                    a.push(x.id);
                    return a;
                }, []);
            return resolve({ ...userData, id: user.id });
        })
});

export const getVkProfile = (id, isArray = false) => new Promise((resolve, reject) => {
    const callback = ({ detail: { type, data } }) => {
        if (type === "VKWebAppAccessTokenReceived") {
            connect.send("VKWebAppCallAPIMethod", {
                method: "users.get",
                params: {
                    access_token: data.access_token,
                    user_ids: id,
                    fields: "first_name,last_name,photo_200",
                    v: "5.95"
                }
            });
        }
        if (type === "VKWebAppCallAPIMethodResult") {
            connect.unsubscribe(callback);
            return resolve(isArray ? data.response : data.response[0]);
        }
        if (type === "VKWebAppAccessTokenFailed" || type === "VKWebAppCallAPIMethodFailed") {
            connect.unsubscribe(callback);
            return reject();
        }
    };
    connect.subscribe(callback);

    const query_params = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&").reduce((a, x) => {
        const data = x.split("=");
        a[data[0]] = data[1];
        return a;
    }, {});

    connect.send("VKWebAppGetAuthToken", { app_id: parseInt(query_params["vk_app_id"]), scope: "friends" });
});

export const joinByInvite = (invite_code) => new Promise((resolve, reject) => {
    const func = functions.httpsCallable("joinByInvite");
    func({ invite_code })
        .then((result) => resolve(result.data.service))
        .catch((e) => reject(e))
});

export const joinToTesting = (service_id) => new Promise((resolve, reject) => {
    const func = functions.httpsCallable("joinToTesting");
    func({ service_id })
        .then((res) => {
            if(res.data.status === "ok") return resolve();
            return reject();
        })
        .catch((e) => {
            console.error(e);
            reject();
        })
});

export const leaveFromTesting = (id) => new Promise((resolve, reject) => {
    const func = functions.httpsCallable("leaveFromTesting");
    func({ id })
        .then(() => resolve())
        .catch(() => reject())
});

/*
    Services section
 */
export const getServices = (myServices = []) => new Promise(async (resolve) => {
    const promises = [];
    myServices.forEach((serviceId) => promises.push(
        firestore.collection("services").doc(serviceId).get()
            .then(async (service) => {
                const data = { ...service.data(), id: service.id};
                data["joined"] = true;
                data["developer"] = await getDeveloperById(data.developer.id);
                return data;
            })
    ));

    await firestore.collection("services").where("public", "==", true).get()
        .then((allServicesRefs) => {
            allServicesRefs.forEach((service) => {
                if (myServices.findIndex((x) => x === service.id) === -1) {
                    promises.push(new Promise(async (resolve) => {
                        const data = { ...service.data(), id: service.id};
                        data["developer"] = await getDeveloperById(data.developer.id);
                        resolve(data);
                    }))
                }
            })
        });

    resolve(await Promise.all(promises));
});

export const getMembers = (service_id) => new Promise((resolve) => {
    const service = firestore.collection("services").doc(service_id);
    firestore.collection("users").where("services", "array-contains", service).get()
        .then(async (users) => {
            const profileIds = [];
            users.forEach((users) => profileIds.push(users.id));
            return resolve(await getVkProfile(profileIds, true));
        })
        .catch(() => resolve([]));
});

export const kickMember = (service_id, member_id) => new Promise((resolve, reject) => {
    const func = functions.httpsCallable("kickMember");
    func({ service_id, member_id })
        .then((res) => {
            if(res.data.status === "ok") return resolve();
            return reject();
        })
        .catch((e) => {
            console.error(e);
            return reject();
        })
});

export const getServiceById = (id) => new Promise((resolve, reject) => {
    const ref = firestore.collection("services").doc(id).get();
    ref
        .then((service) => {
            if (service.exists) {
                resolve({ ...service.data(), id: service.id });
            } else {
                reject();
            }
        })
        .catch(() => reject())
});

export const getServicesByDeveloper = (developer_id) => new Promise((resolve) => {
    const developer = firestore.collection("developers").doc(developer_id);
    firestore.collection("services")
        .where("developer", "==", developer)
        .where("public", "==", true)
        .get()
        .then((services) => {
            const result = [];
            services.forEach((service) => result.push({ ...service.data(), id: service.id }));
            return resolve(result);
        })
        .catch((e) => {
            console.error(e);
            return resolve([]);
        })
});

export const createService = (service) => new Promise((resolve, reject) => {
    const func = functions.httpsCallable("createService");
    func({ service })
        .then((result) => {
            if (result.data && result.data.status === "ok") resolve(result.data.service_id);
            else reject();
        })
        .catch((e) => {
            console.error(e);
            return reject();
        })
});

export const updateService = (service) => new Promise((resolve, reject) => {
    if (!auth.currentUser) return reject();

    const allowedFields = ["developer", "icon", "name", "version", "link", "chat_url"];
    let filteredService = Object.keys(service).filter((x) => allowedFields.indexOf(x) !== -1)
        .reduce((a, x) => { a[x] = x !=="developer" ? service[x] : firestore.collection("developers").doc(service.developer.id); return a; }, {});

    firestore.collection("services").doc(service.id).update(filteredService)
        .then(() => resolve())
        .catch((e) => reject(e));
});

/*
    Versions section
 */
export const getVersions = (service_id) => new Promise((resolve) => {
    firestore.collection("services").doc(service_id).collection("versions").get()
        .then((versions) => {
            const result = {};
            versions.forEach((version) => result[version.id] = { ...version.data(), id: version.id });
            return resolve(result);
        })
        .catch((e) => {
            console.error(e);
            resolve([]);
        })
});

export const createVersion = (service_id, version_id, versionData) => new Promise((resolve, reject) => {
    firestore.collection("services").doc(service_id).collection("versions").doc(version_id).set(versionData)
        .then(() => resolve())
        .catch((e) => {
            console.error(e);
            reject();
        });
});

/*
    Reports section
 */
export const getReports = (service_id) => new Promise((resolve, reject) => {
    const reportsRef = firestore.collection("services").doc(service_id).collection("reports").get();
    reportsRef
        .then((reports) => {
            if (reports.empty) return resolve([]);
            const result = [];
            reports.forEach((report) => result.push({ ...report.data(), id: report.id }));
            resolve(result.sort((a, b) => b.created_at.seconds - a.created_at.seconds));
        })
        .catch(() => reject());
});

export const postReport = (report, service_id) => new Promise((resolve, reject) => {
    if (!auth.currentUser) return reject();
    report.creator = auth.currentUser.uid;
    report.status = "open";
    report.created_at = firebase.firestore.FieldValue.serverTimestamp();

    firestore.collection("services").doc(service_id).collection("reports").add(report)
        .then((reportRef) => {
            reportRef.get()
                .then((report) => resolve({ ...report.data(), id: report.id }))
                .catch((e) => reject(e));
        })
        .catch((e) => reject(e));
});

export const updateReport = (report, service_id) => new Promise((resolve, reject) => {
    if (!auth.currentUser) return reject();

    const allowedFields = ["creator", "created_at", "title", "steps", "actual_result", "expected_result", "type", "priority", "version", "status"];
    const filteredReport = Object.keys(report).filter((x) => allowedFields.indexOf(x) !== -1)
        .reduce((a, x) => { a[x] = report[x]; return a; }, {});

    firestore.collection("services").doc(service_id).collection("reports").doc(report.id).update(filteredReport)
        .then(() => resolve())
        .catch((e) => reject(e));
});

export const deleteReport = (service_id, report_id) => new Promise((resolve, reject) => {
    firestore.collection("services").doc(service_id).collection("reports").doc(report_id).delete()
        .then(() => resolve())
        .catch((e) => reject(e))
});

export const isMyReport = (creator_id) => creator_id === auth.currentUser.uid;

/*
    Invites section
 */

export const getInvites = (service_id) => new Promise((resolve) => {
    firestore.collection("invites").where("service", "==", service_id).get()
        .then((invites) => {
            const result = [];
            invites.forEach((invite) => result.push({ ...invite.data(), id: invite.id }));
            return resolve(result);
        })
        .catch((e) => {
            console.error(e);
            return resolve([]);
        })
});

export const createInvite = (service_id, invite) => new Promise((resolve, reject) => {
    firestore.collection("invites").add(invite)
        .then((invite) => resolve(invite.id))
        .catch((e) => {
            console.error(e);
            return reject();
        })
});

export const deleteInvite = (invite_id) => new Promise((resolve, reject) => {
    firestore.collection("invites").doc(invite_id).delete()
        .then(() => resolve())
        .catch((e) => {
            console.error(e);
            return reject();
        })
});

/*
    Developer section
 */
export const getDeveloperById = (id) => new Promise((resolve) => {
    const ref = firestore.collection("developers").doc(id).get();
    ref.then((doc) => resolve({ ...doc.data(), id: doc.id }));
});

export const getDeveloperStaff = (developer_id) => new Promise((resolve) => {
    const developer = firestore.collection("developers").doc(developer_id);
    firestore.collection("users").where("developer", "==", developer).get()
        .then(async (users) => {
            const usersResult = [];
            users.forEach((user) => usersResult.push(user.id));
            return resolve(await getVkProfile(usersResult, true));
        })
        .catch((e) => {
            console.error(e);
            return resolve([]);
        })
});

export const updateDeveloper = (developer, developer_id) => new Promise((resolve, reject) => {
    firestore.collection("developers").doc(developer_id).update(developer)
        .then(() => resolve())
        .catch((e) => {
            console.error(e);
            return reject();
        })
});