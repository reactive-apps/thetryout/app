import { getServicesByDeveloper, getDeveloperStaff, updateDeveloper } from "../api";

const developer = {
    state: {},
    reducers: {
        loaded(state, payload) {
            return payload;
        },
        update(state, payload) {
            return { ...state, ...payload };
        }
    },
    effects: (dispatch) => ({
        async selectDeveloper(developer, state) {
            dispatch.navigator.setLoading();
            dispatch.developer.loaded({
                ...developer,
                isMy: state.user.developer ? developer.id === state.user.developer.id : false,
                services: [
                    ...await getServicesByDeveloper(developer.id),
                    ...state.services.list.filter((x) => x.joined && !x.public && x.developer.id === developer.id),
                ],
                staff: await getDeveloperStaff(developer.id)
            });
            dispatch.navigator.goForward("developer");
        },
        updateDeveloper({ developer, successCallback, errorCallback }, state) {
            dispatch.navigator.setLoading();
            updateDeveloper(developer, state.developer.id)
                .then(() => {
                    state.services.list
                        .filter((x) => x.developer.id === state.developer.id)
                        .forEach((service) => dispatch.services.update({
                            id: service.id,
                            developer: { ...service.developer, ...developer }
                        }));
                    dispatch.developer.loaded({ ...state.developer, ...developer });
                    if (state.user.developer && state.developer.id === state.user.developer.id)
                        dispatch.user.update({ developer: { ...state.user.developer, ...developer } });
                    dispatch.navigator.setPopout(null);
                    successCallback();
                })
                .catch((e) => {
                    console.log(e);
                    dispatch.navigator.setPopout(null);
                    errorCallback();
                });
        }
    })
};

export default developer;