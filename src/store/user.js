import { getAccount } from "../api";

const user = {
    state: {},
    reducers: {
        loaded(state, payload) {
            return payload;
        },
        update(state, payload) {
            return { ...state, ...payload };
        }
    },
    effects: (dispatch) => ({
        async load() {
            const user = await getAccount();
            dispatch.user.loaded({ ...user, filter: "my" });
        },
        setFilter(filter) {
            dispatch.user.update({ filter });
        },
        joinService(service, state) {
            dispatch.user.update({ services: [ ...state.user.services, service ] });
        },
        leaveService(service, state) {
            dispatch.user.update({ services: state.user.services.filter((x) => x !== service) });
        }
    })
};

export default user;