import { init } from "@rematch/core";

import navigator from "./navigator";
import services from "./services";
import reports from "./reports";
import user from "./user";
import developer from "./developer";

const models = {
    navigator,
    services,
    reports,
    user,
    developer
};

const store = init({
   models
});

export default store;