import { getReports, getVkProfile, postReport, deleteReport, isMyReport, updateReport } from "../api";

const reports = {
    state: {
        list: [],
        isLoaded: false,
        activeReport: {}
    },
    reducers: {
        setLoading(state) {
            return { ...state, isLoaded: false };
        },
        loaded(state, payload) {
            return { ...state, isLoaded: true, list: payload };
        },
        select(state, payload) {
            return { ...state, activeReport: payload };
        },
        push(state, payload) {
            return { ...state, list: [ { ...payload, isMy: true }, ...state.list ] };
        },
        update(state, payload) {
            const id = state.list.findIndex((x) => x.id === payload.id);
            const updatedList = state.list;
            updatedList[id] = {...state.list[id], ...payload};
            return { ...state, list: updatedList, activeReport: { ...state.activeReport, ...payload } };
        },
        delete(state, payload) {
            return { ...state, list: state.list.filter((x) => x.id !== payload)  };
        }
    },
    effects: (dispatch) => ({
        async load(payload) {
            dispatch.reports.setLoading();
            getReports(payload).then((reports) => dispatch.reports.loaded(reports.map((x) => {
                x.isMy = isMyReport(x.creator);
                return x;
            })));
        },
        selectReport(payload, state) {
            dispatch.navigator.setLoading();
            const id = state.reports.list.findIndex((x) => x.id === payload);
            if(state.reports.list[id].type === "catched-error") {
                dispatch.reports.select({
                    ...state.reports.list[id],
                    service_id: state.services.activeService.id
                });
                dispatch.navigator.goForward("report");
            } else {
                getVkProfile(state.reports.list[id].creator)
                    .then((profile) => {
                        dispatch.reports.select({
                            ...state.reports.list[id],
                            creatorData: profile,
                            service_id: state.services.activeService.id
                        });
                        dispatch.navigator.goForward("report");
                    })
                    .catch(() => {
                        dispatch.reports.select({
                            ...state.reports.list[id],
                            service_id: state.services.activeService.id
                        });
                        dispatch.navigator.goForward("report");
                    })
            }
        },
        deleteReport({ service_id, report_id }) {
            dispatch.navigator.setLoading();
            deleteReport(service_id, report_id)
                .then(() => {
                    dispatch.reports.delete(report_id);
                    dispatch.navigator.goBack();
                })
                .catch((e) => {
                    console.error(e);
                    dispatch.navigator.setPopout(null);
                })
        },
        createReport({ report, service_id, errorCallback }) {
            dispatch.navigator.setLoading();
            postReport(report, service_id)
                .then((report) => {
                    dispatch.reports.push(report);
                    dispatch.navigator.goBack();
                })
                .catch((e) => {
                    console.error(e);
                    dispatch.navigator.setPopout(null);
                    errorCallback();
                })
        },
        updateReport({ report, service_id, errorCallback }) {
            dispatch.navigator.setLoading();
            const report_id = report.id;
            updateReport(report, service_id)
                .then(() => {
                    dispatch.reports.update({ ...report, id: report_id });
                    dispatch.navigator.goBack();
                })
                .catch((e) => {
                    console.error(e);
                    dispatch.navigator.setPopout(null);
                    errorCallback();
                })
        },
        actualReport(report, state) {
            dispatch.navigator.setLoading();
            const updatedReport = report;
            updatedReport.version = state.services.list[state.services.list.findIndex((x) => x.id === report.service_id)].version;

            updateReport(report, report.service_id)
                .then(() => {
                    dispatch.reports.update(report);
                    report.version = updatedReport.version;
                    dispatch.navigator.setPopout(null);
                })
                .catch((e) => {
                    console.error(e);
                    dispatch.navigator.setPopout(null);
                })
        },
        updateStatus({ report, newStatus }) {
            dispatch.navigator.setLoading();
            const updatedReport = report;
            updatedReport.status = newStatus;

            updateReport(report, report.service_id)
                .then(() => {
                    dispatch.reports.update(report);
                    report.status = updatedReport.status;
                    dispatch.navigator.setPopout(null);
                })
                .catch((e) => {
                    console.error(e);
                    dispatch.navigator.setPopout(null);
                })
        }
    })
};

export default reports;