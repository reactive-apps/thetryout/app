import { getServices, joinByInvite, getServiceById, getDeveloperById, leaveFromTesting, getVersions, updateService, createVersion, getInvites, createInvite, deleteInvite, createService, joinToTesting, getMembers, kickMember } from "../api";

const services = {
    state: {
        list: [],
        isLoaded: false,
        activeService: 0
    },
    reducers: {
        setLoading(state) {
            return { ...state, isLoaded: false };
        },
        loaded(state, payload) {
            return { ...state, isLoaded: true, list: payload };
        },
        push(state, payload) {
            return { ...state, list: [ payload, ...state.list ] }
        },
        update(state, payload) {
            const id = state.list.findIndex((x) => x.id === payload.id);
            const updatedList = state.list;
            updatedList[id] = {...state.list[id], ...payload};
            return { ...state, list: updatedList, activeService: { ...state.activeService, ...payload } };
        },
        markAsJoined(state, payload) {
            const id = state.list.findIndex((x) => x.id === payload);
            const updatedList = state.list;
            updatedList[id]["joined"] = true;
            return { ...state, list: updatedList };
        },
        markAsLeaved(state, payload) {
            const id = state.list.findIndex((x) => x.id === payload);
            const updatedList = state.list;
            updatedList[id]["joined"] = false;
            return { ...state, list: updatedList };
        },
        select(state, payload) {
            return { ...state, activeService: payload };
        }
    },
    effects: (dispatch) => ({
        load(payload, state) {
            dispatch.services.setLoading();
            getServices(state.user.services).then((services) => services.reverse()).then((services) => {
                dispatch.services.loaded(services.reduce((a, x) => {
                    if (state.user.developer) {
                        x.isServiceDeveloper = state.user.developer.id === x.developer.id;
                    }
                    a.push(x);
                    return a;
                }, []));
            });
        },
        async selectService(payload, state) {
            dispatch.navigator.setLoading();
            const id = state.services.list.findIndex((x) => x.id === payload);
            dispatch.services.select({
                ...state.services.list[id],
                versions: state.services.list[id].joined || state.services.list[id].isServiceDeveloper ? await getVersions(state.services.list[id].id) : [],
                invites: state.services.list[id].isServiceDeveloper ? await getInvites(state.services.list[id].id) : [],
                members: state.services.list[id].isServiceDeveloper ? await getMembers(state.services.list[id].id) : []
            });
            if(state.services.list[id].joined) dispatch.reports.load(state.services.list[id].id);
            dispatch.user.update({ filter: "my" });
            dispatch.navigator.goForward("service");
        },
        joinByInvite({ invite, errorCallback }, state) {
            dispatch.navigator.setLoading();
            joinByInvite(invite)
                .then((service) => {
                    if (!service) {
                        dispatch.navigator.setPopout(null);
                        return errorCallback();
                    }
                    dispatch.user.joinService(service);
                    if (state.services.list.findIndex((x) => x.id === service) >= 0) {
                        dispatch.services.markAsJoined(service);
                        dispatch.navigator.goBack();
                    } else {
                        getServiceById(service)
                            .then(async (service) => {
                                service["developer"] = await getDeveloperById(service.developer.id);
                                service["isServiceDeveloper"] = service["developer"] && service["developer"].id === state.user.developer;
                                dispatch.services.push({...service, joined: true});
                                dispatch.navigator.goBack();
                            });
                    }
                })
        },
        join(id) {
            dispatch.navigator.setLoading();
            joinToTesting(id)
                .then(async () => {
                    dispatch.services.update({
                        id,
                        versions: await getVersions(id),
                        joined: true
                    });
                    dispatch.reports.load(id);
                    dispatch.user.joinService(id);
                    dispatch.navigator.setPopout(null);
                })
                .catch(() => dispatch.navigator.setPopout(null))
        },
        leave(id) {
            dispatch.navigator.setLoading();
            leaveFromTesting(id)
                .then(() => {
                    dispatch.services.markAsLeaved(id);
                    dispatch.user.leaveService(id);
                    dispatch.navigator.goBack();
                })
                .catch(() => dispatch.navigator.setPopout(null))
        },
        updateService({ service, successCallback, errorCallback }) {
            dispatch.navigator.setLoading();
            updateService(service)
                .then(() => {
                    dispatch.services.update(service);
                    dispatch.navigator.setPopout(null);
                    successCallback();
                })
                .catch((e) => {
                    console.error(e);
                    dispatch.navigator.setPopout(null);
                    errorCallback();
                })
        },
        createVersion({ version, successCallback, errorCallback }, state) {
            dispatch.navigator.setLoading();
            createVersion(state.services.activeService.id, version.id, { text: version.text })
                .then(() => {
                    dispatch.services.updateService({
                        service: {
                            id: state.services.activeService.id,
                            version: version.id,
                            versions: state.services.activeService.versions
                                ? { ...state.services.activeService.versions, [version.id]: { text: version.text } }
                                : { [version.id]: { text: version.text } }
                        },
                        successCallback: () => {
                            dispatch.navigator.setPopout(null);
                            successCallback();
                        },
                        errorCallback: () => {
                            dispatch.navigator.setPopout(null);
                            errorCallback();
                        }
                    });

                })
                .catch(() => {
                    dispatch.navigator.setPopout(null);
                    errorCallback();
                });
        },
        createInvite({ invite, successCallback, errorCallback }, state) {
            dispatch.navigator.setLoading();
            const payload = {
                service: state.services.activeService.id,
                ...invite
            };
            createInvite(state.services.activeService.id, payload)
                .then((invite_id) => {
                    dispatch.services.update({
                        id: state.services.activeService.id,
                        invites: [ { ...payload, id: invite_id }, ...state.services.activeService.invites ]
                    });
                    dispatch.navigator.setPopout(null);
                    successCallback();
                })
                .catch(() => {
                    dispatch.navigator.setPopout(null);
                    errorCallback();
                })
        },
        deleteInvite(invite_id, state) {
            dispatch.navigator.setLoading();
            deleteInvite(invite_id)
                .then(() => {
                    dispatch.services.update({
                        id: state.services.activeService.id,
                        invites: state.services.activeService.invites.filter((x) => x.id !== invite_id)
                    });
                    dispatch.navigator.setPopout(null);
                })
                .catch(() => {
                    dispatch.navigator.setPopout(null);
                })
        },
        createService({ service, errorCallback }, state) {
            dispatch.navigator.setLoading();
            createService(service)
                .then((service_id) => {
                    const payload = {
                        id: service_id,
                        name: service.name,
                        icon: service.icon,
                        link: service.link,
                        isServiceDeveloper: true,
                        public: false,
                        version: "1.0.0",
                        developer: state.developer,
                        joined: true
                    };
                    dispatch.services.push(payload);
                    dispatch.developer.update({ services: [ payload, ...state.developer.services ] });
                    dispatch.navigator.goBack();
                })
                .catch((e) => {
                    console.error(e);
                    dispatch.navigator.setPopout(null);
                    errorCallback();
                })
        },
        async kickMember(member_id, state) {
            await dispatch.navigator.setLoading();
            kickMember(state.services.activeService.id, member_id)
                .then(() => {
                    dispatch.services.update({
                        id: state.services.activeService.id,
                        members: state.services.activeService.members.filter((x) => x.id !== member_id)
                    });
                    dispatch.navigator.setPopout(null);
                })
                .catch(() => {
                    dispatch.navigator.setPopout(null);
                })
        }
    })
};

export default services;