import React, { useState } from "react";
import VKConnect from "@vkontakte/vkui-connect";
import { connect } from "react-redux";
import { View, ConfigProvider } from "@vkontakte/vkui";

import InitPanel from "./panels/Init";
import HomePanel from "./panels/Home";
import JoinPanel from "./panels/Join";
import ServicePanel from "./panels/Service";
import ReportPanel from "./panels/Report";
import ReportEditor from "./panels/ReportEditor";
import StatusHelp from "./panels/StatusHelp";
import DeveloperPanel from "./panels/Developer";
import CreateService from "./panels/CreateService";

const App = ({ activePanel, history, popout, goBack }) => {
	const [ scheme, setScheme ] = useState("client_light");

	VKConnect.subscribe(({ detail: { type, data } }) => type === "VKWebAppUpdateConfig" ? setScheme(data.scheme) : null);

	return (
		<ConfigProvider isWebView scheme={scheme}>
			<View
				activePanel={activePanel}
				history={history}
				popout={popout}
				onSwipeBack={goBack}
			>
				<InitPanel id="init"/>
				<HomePanel id="home"/>
				<JoinPanel id="join"/>
				<ServicePanel id="service"/>
				<DeveloperPanel id="developer"/>
				<CreateService id="create-service"/>
				<ReportEditor.create id="create-report"/>
				<ReportPanel id="report"/>
				<StatusHelp id="status-help"/>
				<ReportEditor.update id="update-report"/>
			</View>
		</ConfigProvider>
	)
};

const mapState = (state) => ({
	activePanel: state.navigator.active,
	history: state.navigator.history,
	popout: state.navigator.popout
});

const mapDispatch = (dispatch) => ({
	goBack: dispatch.navigator.goBack
});

export default connect(mapState, mapDispatch)(App);
