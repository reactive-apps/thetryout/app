<img src="./docs/banner.jpg" alt="TryOut Banner">

# TryOut for VK Apps 🧪
Platform for testing of VK Apps services.

## Open App
For view this app you need to open this [link](https://vk.com/app7007699)

## Technologies
*   React
*   Redux / react-redux
*   Rematch
*   VKUI
*   VKUI Connect / VKUI Connect Promise
*   Firebase
*   Github Actions

## Developers
This project developing by Reactive Apps Team:
*   [Stepan Novozhilov](https://vk.me/this.state.user)

## Links
*   [Reactive Apps in VK](https://vk.com/reactapps)
*   [App in VK](https://vk.com/app7007699)